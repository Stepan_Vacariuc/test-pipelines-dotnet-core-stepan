using System;
using Xunit;
using BitbucketPipelines.Lib;

namespace BitbucketPipelines.Unit.Test
{
    public class ProviderTests
    {
        [Fact]
        public void ProviderSubtractShouldWork()
        {
            Assert.Equal(42, new Provider().Substract(44, 2));
        }
    }
}
