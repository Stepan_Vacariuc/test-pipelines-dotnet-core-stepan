using System;
using BitbucketPipelines.Lib;
using Xunit;

namespace BitbucketPipelines.Test
{
    public class ProviderTests
    {
        [Fact]
        public void ProviderSumWorks()
        {
            Assert.Equal(42, new Provider().Sum(40, 3));
        }
    }
}
